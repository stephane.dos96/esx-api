package com.thaku.esxapi.esxapi.entity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Generated;

import java.sql.Timestamp;

@Data
@EqualsAndHashCode
@Generated
@Builder
public class Messages {
    private Integer id;
    private String transmitter;
    private String receiver;
    private String message;
    private String time;
    private Integer isRead;
    private Integer owner;
}
