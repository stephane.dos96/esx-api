package com.thaku.esxapi.esxapi.entity;


import lombok.*;

@Data
@EqualsAndHashCode
@Generated
@Builder
public class Xplayer {

    private String name;
    private String job;
    private String jobgrade;
    private Integer money;
    private Integer Bank;
    private Integer number;
}
