package com.thaku.esxapi.esxapi.dao;

import com.thaku.esxapi.esxapi.entity.Messages;

import java.sql.*;
import java.util.ArrayList;

public class MessagesDao {
    private Connection connect = null;
    String url = "jdbc:mysql://localhost:8889/lscityv3";
    String utilisateur = "admin";
    String motDePasse = "admin";
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    public ArrayList<Messages> getMessage(String transmitter,String receiver) throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(url, utilisateur, motDePasse);
            statement = connect.createStatement();
            String query="select * from phone_messages where transmitter='"+transmitter+"'";
            resultSet = statement.executeQuery(query);
            ArrayList<Messages> messagesList = new ArrayList<>();
            while (resultSet.next()) {
                Messages sms= Messages.builder()
                        .isRead(resultSet.getInt("isRead"))
                        .message(resultSet.getString("message"))
                        .owner(resultSet.getInt("owner"))
                        .receiver(resultSet.getString("receiver"))
                        .transmitter(resultSet.getString("transmitter"))
                        .time(resultSet.getString("time"))
                        .build();
                messagesList.add(sms);
            }
            return messagesList;

        } catch (Exception e) {
            throw e;
        } finally {

        }
    }
}
