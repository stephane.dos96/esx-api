package com.thaku.esxapi.esxapi.controler;

import com.thaku.esxapi.esxapi.dao.XplayerDao;
import com.thaku.esxapi.esxapi.entity.Xplayer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/xplayer")
@CrossOrigin(origins = {"http://localhost:4500"}, maxAge = 4800, allowCredentials = "false")
@RestController
@Slf4j
public class XplayerControler {


    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"}, value = "/steam/{steam}")
    public  ResponseEntity<Xplayer> getXplayerData(@PathVariable final String steam) throws Exception {
        System.out.println("LOL");
        System.out.println(steam);
        String steamId="steam:"+steam;
        XplayerDao dao = new XplayerDao();
        Xplayer xplayer = dao.readDataBase(steamId);
        return new ResponseEntity<>(xplayer, HttpStatus.OK);

    }

}
