package com.thaku.esxapi.esxapi.controler;


import com.thaku.esxapi.esxapi.dao.MessagesDao;
import com.thaku.esxapi.esxapi.dao.XplayerDao;
import com.thaku.esxapi.esxapi.entity.Messages;
import com.thaku.esxapi.esxapi.entity.Xplayer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RequestMapping(value = "/messages")
@CrossOrigin(origins = {"http://localhost:4500"}, maxAge = 4800, allowCredentials = "false")
@RestController
@Slf4j
public class MessagesControler {

    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"}, value = "/message/{transmitter}/{receiver}")
    public ResponseEntity< ArrayList<Messages>> getMessages(@PathVariable final String transmitter, final String receiver) throws Exception {
        System.out.println("sa passe");
        MessagesDao dao = new MessagesDao();
        ArrayList<Messages> messageList = dao.getMessage(transmitter,receiver);
        return new ResponseEntity<>(messageList, HttpStatus.OK);

    }

}
