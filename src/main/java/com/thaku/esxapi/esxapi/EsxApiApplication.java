package com.thaku.esxapi.esxapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsxApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(EsxApiApplication.class, args);
    }
}
